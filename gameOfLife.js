const initialPercentageLiving = 20;
const rowsNum = 30;
const columnsNum = 30;
const intervalTime = 2000;
const numNeigboursBornCondition = 3;
const isolationCondition = 2;
const overpopulationCondition = 4;
const board = document.getElementById("myBoard");
let allCells = [];

class Cell {
    constructor(row, column) {
        this.row = row;
        this.column = column;
    } 


    findCell(row, column, allCells) {
        if (row<0 || row>rowsNum-1 || column<0 || column>rowsNum-1) {
            return this;
        } else {
            let foundCell = allCells.filter(cell => cell.row===row && cell.column===column);
            return foundCell[0];
        } 
        
    }

    countNeighbours(allCells) {
        let counter = 0;
        for (let i=-1; i<2; i++) {
            for (let j=-1; j<2; j++) {
                let neighbourCell = this.findCell((this.row+i), (this.column+j), allCells);
                if (neighbourCell!=this && counter<5 && neighbourCell.constructor===LivingCell) {
                    counter++;
                } 
            }
        }
        return counter;
    }
}

class DeadCell extends Cell {
    constructor(row, column) {
        super(row, column);
         board.getElementsByTagName("tr")[this.row].getElementsByTagName("td")[this.column].classList.remove("living");
    }

    next(neighbourNum) {
        if(neighbourNum===numNeigboursBornCondition) {
            return new LivingCell(this.row, this.column);
        } else {
            return this;
        }
    }

}

class LivingCell extends Cell {
    constructor(row, column) {
        super(row, column);
        board.getElementsByTagName("tr")[this.row].getElementsByTagName("td")[this.column].classList.add("living");
    }

    next(neighbourNum) {
        if (neighbourNum< isolationCondition || neighbourNum> overpopulationCondition) {
            return new DeadCell(this.row, this.column);
        } else {
            return this;
        }
    }
}

createBoard = function() {
    for (let i=0; i<rowsNum; i++) {
        let myRow = board.insertRow(i);
        for (let j=0; j<columnsNum; j++) {
            let singleCell=myRow.insertCell(j);
        }
    }
}

createCells = function(row, column){
    if (Math.random()*100 <initialPercentageLiving) {
        return new LivingCell(row, column);
    } else {
        return new DeadCell(row, column);
    }
}

populateBoard = function() {
    for (let i=0; i<rowsNum*columnsNum; i++) {
        const currentRow = Math.floor(i/columnsNum);
        const currentColumn = i-currentRow*columnsNum;
        allCells.push(createCells(currentRow, currentColumn));
    }
}

iterate = function() {
    setInterval(function() {
        const neighboursArr = allCells.map(element => element.countNeighbours(allCells));
        let tempArr = [];
        for (let m = 0; m<rowsNum*columnsNum; m++) {
            tempArr.push(allCells[m].next(neighboursArr[m]));
        }
        allCells = tempArr;
    }, intervalTime);
}

createBoard();
populateBoard();
iterate();
